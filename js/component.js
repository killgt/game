define(['class'], function(Class) {
	return Class.extend({
		entity : null,
		updatable : false,
		init : function(updatable){
			this.updatable = updatable;
		},
		onAdd : function(entity){
			this.entity = entity;
		}
	});
});