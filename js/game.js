define(
	[
	'class',
	'systems/rendersystem',
	'systems/physicsystem',
	'entity',
	'components/image',
	'components/randomposition',
	'components/control',
	'components/moto',
	],
	function(Class, Render, Physic, Entity, ImageComponent, RandomPositionComponent, PlayerControl, Moto) {
	window.requestAnimFrame = (function(callback) {
		return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame ||
		function(callback) {
			window.setTimeout(callback, 1000 / 60);
		};
	})();

	return Class.extend({
		render : null,
		physic : null,
		entities : {},
		delta : 0,
		time : null,
		ids: 1,
		keys : new Array(255),
		iniciar: function() {
			var self = this;
			console.log('Iniciando juego');

			this.render = new Render(this);
			this.physic = new Physic();

			var jugador = new Entity(this, [new Moto(), new PlayerControl()], {
				position : {
					x : 400,
					y : 0
				}
			});
			this.addEntity(jugador);
			this.render.setTarget(jugador);

			//bindeamos los eventos de teclado
			window.onkeydown = function(e){
				self.keys[String.fromCharCode(e.which).toLowerCase()] = true;
			};
			window.onkeyup = function(e){
				self.keys[String.fromCharCode(e.which).toLowerCase()] = false;
			};

			this.gameLoop(this);
			return true;
		},
		addEntity : function(e){
			this.entities[e.id] = e;
		},
		gameLoop : function(self){
			var now = new Date().getTime();
			this.delta = now - (this.time || now);
			this.time = now;

			for(var key in this.entities){
				if (this.entities.hasOwnProperty(key)){
					this.entities[key].update(this.delta);
				}
			}

			self.physic.update(this.delta);
			self.render.update(this.delta);

			window.requestAnimFrame(function(){
				self.gameLoop(self);
			});
		},
		randomLog : function(cosa){
			if (Math.random()*1000 > 960)
				console.log(cosa);
		}
	});
});