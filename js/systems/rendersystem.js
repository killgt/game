define(['class'], function(Class) {
	return Class.extend({
		canvas : null,
		realCanvas : null,
		target : null,
		game: null,
		init : function(game){
			var self = this;
			this.renderables = [];
			this.screen = {
				width : 0,
				height : 0,
			};
			this.camera = {
				x : 0,
				y : 0,
			};

			this.realCanvas = document.createElement('canvas');
			document.querySelector('body').appendChild(this.realCanvas);
			this.game = game;

			this.refreshScreenSize(this);
			window.onresize = function(){
				self.refreshScreenSize(self);
			};
			this.canvas = this.realCanvas.getContext('2d');
		},
		addRenderable: function(renderable){
			this.renderables.push(renderable);
		},
		update : function(){
			var self = this;
			//Limpiamos la pantalla
			self.canvas.clearRect(0, 0, self.screen.width, self.screen.height);

			//Recalculamos la posicion de la camara
			if (self.target){
				self.camera.x = self.target.position.x - self.screen.width / 2;
				self.camera.y = self.target.position.y - self.screen.height / 2;
			}
			//Nos movemos a la camara
			self.canvas.save();
			self.canvas.translate(-self.camera.x, -self.camera.y);

			//renderizamos las entidades
			this.renderables.forEach(function(r){
				r.render(self.canvas);
			});

			//TODO: Condicionar
			this.game.physic.render(self.canvas);

			//dejamos todo como estaba
			self.canvas.restore();
		},
		refreshScreenSize :  function(self){
			self.screen.width = self.realCanvas.offsetWidth;
			self.screen.height = self.realCanvas.offsetHeight;
			self.realCanvas.width = self.realCanvas.offsetWidth;
			self.realCanvas.height = self.realCanvas.offsetHeight;
		},
		setTarget : function(entity){
			this.target = entity;
		}
	});
});