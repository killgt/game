define(['class'], function(Class) {
	return Class.extend({
		world : null,
		init : function(game){
			var self = this;
			this.world = new cp.Space();
			//this.world.iterations = 30;
			this.world.gravity = { x: 0, y: 1000};
			this.world.sleepTimeThreshold = 0.5;
			this.world.collisionSlop = 0.5;

			// añadimos el suelo
			this.proceduralLine({x:0,y:500}, {x:20000, y: 500}, 6, 2000);

			cp.GrooveJoint.prototype.render = function(canvas) {
				var a = this.a.local2World(this.grv_a);
				var b = this.a.local2World(this.grv_b);
				var c = this.b.local2World(this.anchr2);

				canvas.strokeStyle = "grey";
				canvas.beginPath();
				canvas.moveTo(a.x, a.y);
				canvas.lineTo(b.x, b.y);
				canvas.stroke();

			};
			cp.PinJoint.prototype.render = function(canvas) {
				var a = this.a.local2World(this.anchr1);
				var b = this.b.local2World(this.anchr2);

				canvas.strokeStyle = "grey";
				canvas.beginPath();
				canvas.moveTo(a.x, a.y);
				canvas.lineTo(b.x, b.y);
				canvas.stroke();

			};

			cp.BoxShape.prototype.render = function(canvas){
				canvas.strokeStyle = "white";
				canvas.beginPath();
				var verts = this.body.shapeList[0].tVerts;
				var len = verts.length;
				var lastPoint = new cp.Vect(verts[len - 2], verts[len - 1]);
				canvas.moveTo(lastPoint.x, lastPoint.y);

				for (var i = 0; i < len; i += 2) {
					var p = (new cp.Vect(verts[i], verts[i + 1]));
					canvas.lineTo(p.x, p.y);
				}

				canvas.stroke();
			};

			cp.PolyShape.prototype.render = function(canvas){

				canvas.strokeStyle = "white";
				canvas.beginPath();

				var verts = this.body.shapeList[0].tVerts;
				var len = verts.length;
				var lastPoint = new cp.Vect(verts[len - 2], verts[len - 1]);
				canvas.moveTo(lastPoint.x, lastPoint.y);

				for (var i = 0; i < len; i += 2) {
					var p = (new cp.Vect(verts[i], verts[i + 1]));
					canvas.lineTo(p.x, p.y);
				}

				canvas.stroke();
			};

			cp.SegmentShape.prototype.render = function(canvas){
				canvas.strokeStyle = "white";
				canvas.beginPath();

				canvas.moveTo(this.ta.x, this.ta.y);
				canvas.lineTo(this.tb.x, this.tb.y);

				canvas.stroke();
			};

			cp.CircleShape.prototype.render = function(canvas) {
				var radio = this.body.shapeList[0].r;
				var angulo = Math.acos(this.body.rot.x, this.body.rot.y)*2 - Math.PI;

				canvas.beginPath();
				canvas.arc(this.body.p.x, this.body.p.y, radio, 0, 2*Math.PI, false);
				canvas.stroke();

				var l = cp.v.mult(this.body.rot, radio).add(this.body.shapeList[0].tc);
				canvas.beginPath();
				canvas.lineTo(this.body.p.x, this.body.p.y);
				canvas.lineTo(l.x, l.y);
				canvas.stroke();
			};
		},
		update : function(delta){
			this.world.step(1 / (1000/delta));

			this.world.eachShape(function (shape) {
		
			});
		},
		render : function(canvas){
			this.world.eachShape(function (shape) {
				if (typeof shape.render != 'undefined')
					shape.render(canvas);
			});
			this.world.eachConstraint(function (constraint) {
				if (typeof constraint.render != 'undefined')
					constraint.render(canvas);
			});
		},
		getBox : function(x, y, width, height, density, friction){
			density = density || 1;
			friction = friction || 0.5;

			var mass = width * height * density;
			var moment = cp.momentForBox(mass, width, height);
			var body = this.world.addBody(new cp.Body(mass, moment));
			body.setPos({x:x, y:y});
			var shape = this.world.addShape(new cp.BoxShape(body, width, height));
			shape.setFriction(friction);
			shape.group = 1;

			console.log('Añadiendo caja:', 'X:' + x, 'Y:' + y, 'W:' + width, 'H:' + height, 'D:' + density, 'F:' + friction, 'M:' + mass);
			return body;
		},
		getCircle : function(x, y, radius, density, friction, elasticity){
			density = density || 0.3;
			elasticity = elasticity || 0.9;
			friction = friction || 1;
			mass = radius * density;

			var body = this.world.addBody(new cp.Body(mass, cp.momentForCircle(mass, 0, radius, {x:0, y:0})));
			body.setPos({x:x, y:y});
			var circle = this.world.addShape(new cp.CircleShape(body, radius, {x:0, y:0}));
			circle.group = 1;
			circle.setElasticity(elasticity);
			circle.setFriction(friction);


			console.log('Masa rueda', mass);
			return body;
		},
		addLine : function(from, to, friction, elasticity){
			var linea = this.world.addShape(new cp.SegmentShape(this.world.staticBody, from, to, 0));
			linea.setFriction(friction || 1);
			linea.setElasticity(elasticity || 1);

		},
		proceduralLine : function(from, to, levels, height){
			var puntos = [];

			function intermedio(f, t){
				return {x:f.x + (t.x - f.x)/2, y:f.y + (t.y - f.y)/2};
			}

			puntos.push(from);
			puntos.push(to);
			
			var sorter = function(a,b){
				return a.x - b.x;
			};

			for (var i = 0; i < levels; i++) {
				var temp = puntos.slice(0);
				var des = height/levels;

				for (var u = 0; u < temp.length-1; u++) {
					var mid = intermedio(temp[u], temp[u+1]);
					mid.y += (Math.random()*des)-des/2;
					puntos.push(mid);
				}
				puntos.sort(sorter);
			}

			puntos.sort(sorter);


			for (var a = 0; a < puntos.length-1; a++) {
				var floor = this.world.addShape(new cp.SegmentShape(this.world.staticBody, puntos[a], puntos[a+1], 0));
				floor.setElasticity(1);
				floor.setFriction(1);
			}
		}
	});
});