define(['class'], function(Class) {
	return Class.extend({
		//global data
		id : 0,
		game : null,
		rotation : 0,
		init : function(game, entidades, options) {
			this.game = game;
			this.id = (this.game.ids++);
			var self = this;

			this.position = {
				x : 0,
				y: 0,
			};
			this.velocity = {
				x : 0,
				y: 0,
			};
			this.size = {
				width : 0,
				height: 0,
			};
			if (options)
				for (var key in options)
					this[key] = options[key];

			if (typeof entidades != 'undefined')
				entidades.forEach(function(c){
					self.addComponent(c);
				});


		},
		// Component system
		components : [],
		addComponent : function(c){
			this.components.push(c);
			c.onAdd(this);
		},
		update : function(delta){
			this.components.forEach(function(c){
				if (c.updatable)
					c.update(delta);
			});
			this.position.x += this.velocity.x * delta;
			this.position.y += this.velocity.y * delta;
		},
		// Tag system
		tags : [],
		addTag : function(t){
			this.tags.push(t);
		},
		hasTag : function(t){
			return this.tags.indexOf(t) >= 0;
		}
	});
});