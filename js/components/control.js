define(['component'], function(Component) {
	return Component.extend({
		maxSpeed : 0.2,
		aceleration : 0.0005,
		init : function(maxSpeed, aceleration){
			this._super(true);
			this.maxSpeed = maxSpeed || this.maxSpeed;
			this.aceleration = aceleration || this.aceleration;

			this.rotable = gyro.hasFeature('deviceorientation');
		},
		onAdd : function(entity){
			this._super(entity);
			var self = this;
		},
		update : function(delta) {
			var velocidad = Math.sqrt(this.entity.velocity.x * this.entity.velocity.x +
				this.entity.velocity.y * this.entity.velocity.y);

			var limite = velocidad > this.maxSpeed;

			var direccion = {
				x : Math.cos((this.entity.rotation + 90) * Math.PI / 180),
				y : Math.sin((this.entity.rotation + 90) * Math.PI / 180),
			};

			if (this.entity.game.keys['w']){
				this.entity.velocity.y += direccion.y * this.aceleration * delta;
				this.entity.velocity.x += direccion.x * this.aceleration * delta;
			}
			if (this.entity.game.keys['a'])
				this.entity.rotation -= delta*0.3;
			if (this.entity.game.keys['d'])
				this.entity.rotation += delta*0.3;

			if (this.rotable){
				var orientacion = gyro.getOrientation().beta;
				this.entity.rotation = orientacion*2;
			}
		}
	});
});