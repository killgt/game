define(['component'], function(Component) {
	return Component.extend({
		image : null,
		path : null,
		init : function(path){
			this._super(false);
			var self = this;

			this.path = path;
			this.image = new Image();
			this.image.src = this.path;
			this.size = {
				width : 0,
				height : 0
			};
			this.image.onload = function(){
				self.width = this.width;
				self.height = this.height;
				if (self.entity){
					self.entity.size.width = this.width;
					self.entity.size.height = this.height;
				}
			};

		},
		onAdd : function(entity){
			this._super(entity);
			this.entity.size.width = this.width;
			this.entity.size.height = this.height;
			this.entity.game.render.addRenderable(this);
		},
		render : function(canvas){
			canvas.save();
			canvas.translate(this.entity.position.x, this.entity.position.y);
			canvas.rotate(this.entity.rotation * Math.PI / 180);
			canvas.translate(-parseInt(this.entity.size.width/2), -parseInt(this.entity.size.height/2));
			canvas.drawImage(this.image, 0, 0, this.entity.size.width, this.entity.size.height);
			canvas.restore();
		},
	});
});