define(['component'], function(Component) {
	return Component.extend({
		cuadro: null,
		ruedas : null,
		motor : null,
		suspensiones : null,
		init : function(){
			this._super(true);
		},
		onAdd : function(entity){
			this._super(entity);
			var game = this.entity.game;

			game.render.addRenderable(this);
			this.cuadro = game.physic.getBox(this.entity.position.x, this.entity.position.y, 100, 50, 0.1);
			this.ruedas = {
				trasera : game.physic.getCircle(this.entity.position.x - 80, this.entity.position.y + 50, 30, 5, 50, 0.01),
				delantera : game.physic.getCircle(this.entity.position.x + 80, this.entity.position.y + 40, 30, 5, 50, 0.01)
			};
			this.suspensiones = {
				trasera : new cp.GrooveJoint(this.cuadro, this.ruedas.trasera, {x:-80, y:-50}, {x:-80, y:50}, {x:0,y: 0}),
				delantera : new cp.GrooveJoint(this.cuadro, this.ruedas.delantera, {x:40, y:-20}, {x:80, y:40}, {x:0,y: 0})
			};
			this.muelles = {
				trasera : new cp.DampedSpring(this.cuadro, this.ruedas.trasera, {x:-80, y:-50}, {x:0, y: 0}, 300, 3000, 2500),
				delantera : new cp.DampedSpring(this.cuadro, this.ruedas.delantera, {x:40, y:-20}, {x:0, y: 0}, 400, 2500, 2500)
			};

			this.motor = new cp.SimpleMotor(this.ruedas.trasera, this.cuadro, 0);
			
			this.motor.on = function(e, speed){
				e.cuadro.activate();
				if (speed)
					this.rate = speed;
				this.maxForce = 40000000;
			};
			this.motor.off = function(){
				this.rate = 0;
				this.maxForce = 1000000;
			};
			game.physic.world.addConstraint(this.motor);

			game.physic.world.addConstraint(this.suspensiones.trasera);
			game.physic.world.addConstraint(this.suspensiones.delantera);
			game.physic.world.addConstraint(this.muelles.trasera);
			game.physic.world.addConstraint(this.muelles.delantera);

			this.entity.position = this.cuadro.p;
		},
		update : function(delta){

			if (this.entity.game.keys['a'])
				this.motor.on(this, -Math.PI*5);
				//this.motor.rate = -Math.PI*4;
			else if (this.entity.game.keys['d'])
				this.motor.on(this, Math.PI*5);
			else
				this.motor.off();

		},
		render : function(canvas){

		},
		renderCuadro : function(canvas){
		},
		renderRueda : function(canvas, rueda){

		}
	});
});