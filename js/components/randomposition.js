define(['component'], function(Component) {
	return Component.extend({
		init : function(path){
			this._super(true);
		},
		onAdd : function(entity){
			this._super(entity);
		},
		update : function(delta) {
			this.entity.position.x = Math.floor((Math.random()*799)+1);
			this.entity.position.y = Math.floor((Math.random()*500)+1);
		}
	});
});